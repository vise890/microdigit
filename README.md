# microdigit

## Install

```bash
cargo install microdigit
```

## Usage

```bash
➜  ~ microdigit 123-456
# ¹²³⁻⁴⁵⁶
➜  ~ microdigit --subscript 123-456
# ₁₂₃₋₄₅₆
➜  ~ microdigit --help
# Microdigit
# Martino V. <martino@visint.in>
# Converts digits into smaller digits. You know, for terminals and things.
#
# USAGE:
#     microdigit [FLAGS] <DIGITS>...
#
# FLAGS:
#     -h, --help         Prints help information
#     -s, --subscript    Converts digits to subscript format
#     -V, --version      Prints version information
#
# ARGS:
#     <DIGITS>...    The digits to convert
```
