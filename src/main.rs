extern crate clap;

use std::process;

use clap::{App, Arg};

mod lib;

pub fn main() {
    let matches = App::new("Microdigit")
        .author("Martino V. <martino@visint.in>")
        .about("Converts digits into smaller digits. You know, for terminals and things.")
        .arg(
            Arg::with_name("subscript")
                .long("subscript")
                .alias("sub")
                .short("s")
                .help("Converts digits to subscript format"),
        )
        .arg(
            Arg::with_name("DIGITS")
                .help("The digits to convert")
                .required(true)
                .multiple(true)
                .index(1),
        )
        .get_matches();

    let subscript = matches.is_present("subscript");
    let digits: String = matches
        .values_of("DIGITS")
        .map(|itr| itr.collect())
        .unwrap();

    if subscript {
        microdigitize_with(lib::microdigitize_subscript, &digits);
    } else {
        microdigitize_with(lib::microdigitize_superscript, &digits);
    }
}

fn microdigitize_with(f: fn(&str) -> Result<String, lib::Error>, input: &str) {
    match f(input) {
        Ok(conv) => println!("{}", conv),
        Err(lib::Error::InvalidInputCharacter(c)) => {
            eprintln!("Invalid input character {}", c);
            process::exit(1);
        }
    }
}
