use phf::phf_map;

static DIGITS_SUPERSCRIPT: phf::Map<char, char> = phf_map! {
    '0' => '⁰',
    '1' => '¹',
    '2' => '²',
    '3' => '³',
    '4' => '⁴',
    '5' => '⁵',
    '6' => '⁶',
    '7' => '⁷',
    '8' => '⁸',
    '9' => '⁹',
    '+' => '⁺',
    '-' => '⁻',
    '=' => '⁼',
    '(' => '⁽',
    ')' => '⁾',
};

static DIGITS_SUBSCRIPT: phf::Map<char, char> = phf_map! {
    '0' => '₀',
    '1' => '₁',
    '2' => '₂',
    '3' => '₃',
    '4' => '₄',
    '5' => '₅',
    '6' => '₆',
    '7' => '₇',
    '8' => '₈',
    '9' => '₉',
    '+' => '₊',
    '-' => '₋',
    '=' => '₌',
    '(' => '₍',
    ')' => '₎',
};

#[derive(Debug, PartialEq)]
pub enum Error {
    InvalidInputCharacter(char),
}

pub fn microdigitize_superscript(s: &str) -> Result<String, Error> {
    microdigitize(&DIGITS_SUPERSCRIPT, s)
}

pub fn microdigitize_subscript(s: &str) -> Result<String, Error> {
    microdigitize(&DIGITS_SUBSCRIPT, s)
}

fn microdigitize(char_map: &phf::Map<char, char>, s: &str) -> Result<String, Error> {
    let mut out: Vec<&char> = Vec::new();
    for c in s.chars() {
        match char_map.get(&c) {
            Some(c) => out.push(c),
            None => return Err(Error::InvalidInputCharacter(c)),
        }
    }
    Ok(out.into_iter().collect())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_microdigitize_superscript() {
        assert_eq!(
            microdigitize_superscript("123-(456)=+"),
            Ok("¹²³⁻⁽⁴⁵⁶⁾⁼⁺".to_string())
        );
        assert_eq!(
            microdigitize_superscript("12x"),
            Err(Error::InvalidInputCharacter('x'))
        );
    }

    #[test]
    fn test_microdigitize_subscript() {
        assert_eq!(
            microdigitize_subscript("123-(456)=+"),
            Ok("₁₂₃₋₍₄₅₆₎₌₊".to_string())
        );
        assert_eq!(
            microdigitize_subscript("12x"),
            Err(Error::InvalidInputCharacter('x'))
        );
    }
}
